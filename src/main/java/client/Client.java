package client;

import gui.MainController;
import org.json.JSONObject;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import static utils.Constants.SERVER_PORT;
import static utils.Constants.SERVER_URL;

public class Client  {

    public static Socket socket;
    public static String user;
    private static ObjectOutputStream writer;
    private static ObjectInputStream reader;


    public static void startClient(String username) throws IOException, ClassNotFoundException {
        user = username;
        System.out.println("User: " + username);
        socket = new Socket(SERVER_URL, SERVER_PORT);
        writer = new ObjectOutputStream(socket.getOutputStream());
        reader = new ObjectInputStream(socket.getInputStream());
        writer.writeObject(new JSONObject().put("Flag", "Chatting").put("Username", username).toString());
        writer.flush();
        sendMessageToServer("������������ " + user + " ����� � ���");
        System.out.println("Started session");

        while (true) {
            String serverMessage = (String) reader.readObject();
            JSONObject serverText = new JSONObject(serverMessage);
            MainController.append(serverText.getString("Message"));
        }
    }

    public static void sendMessageToServer(String message) {
        try {
            writer.writeObject(new JSONObject().put("Message", message).put("Flag", "Mailing").toString());
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
